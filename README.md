# Vue 3 decentralized PoC

This PoC demonstrates how to achieve a functional Vue app
composed of distributed parts.

This app consists of two Vue projects, common-elements and master.  
Common-elements is imported at runtime (NOT compile time) into the app.

![screenshot](screenshot.png)

# How to build

```bash
$ make -C master serve
```