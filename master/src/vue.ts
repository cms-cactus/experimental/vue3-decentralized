// This file is not imported by main.ts or its dependencies
// This is on purpose.
// This file is built separately so we can achieve the following boot order:
// - Vue
// - common-elements
// - app
import * as Vue from "vue";
global.Vue = Vue;
