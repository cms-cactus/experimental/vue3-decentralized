module.exports = {
  filenameHashing: false,
  configureWebpack: {
    entry: {
      // check vue.ts for an explanation
      vue: "./src/vue.ts"
    }
  }
};
