module.exports = {
  chainWebpack: config => {
    // Disable splitChunks plugin, all the code goes into one bundle.
    config.optimization.splitChunks().clear();
  }
};
